//pages
import config from '~/config';
import Home from '~/pages/Home';
import BaoCao from '~/pages/BaoCao';
import CaiDat from '~/pages/CaiDat';
import View from '~/pages/View';

//public routes

const publicRoutes = [
    { path: config.routes.view, component: View},
    { path: config.routes.home, component: Home },
    { path: config.routes.baocao, component: BaoCao },
    { path: config.routes.caidat, component: CaiDat },


];
const privateRoutes = [];

export { publicRoutes, privateRoutes };
