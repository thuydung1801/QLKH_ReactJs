/* eslint-disable jsx-a11y/alt-text */
import config from '~/config';
import classNames from 'classnames/bind';
import styles from './Nav.module.scss';
import Menu, { MenuItem } from './Menu';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDashboard } from '@fortawesome/free-solid-svg-icons';
import { faCircleQuestion, faFileExcel, faUser } from '@fortawesome/free-regular-svg-icons';
import { faSlideshare } from '@fortawesome/free-brands-svg-icons';
import { useTranslation } from 'react-i18next';



const cx = classNames.bind(styles);

function Nav() {
    const {t } = useTranslation();

    return (
        <>
                <aside className={cx('aside')}>

                    <div className={cx('lo-go')}>

                        <img
                            src="https://cpad.ask.fm/450/774/576/-29996968-1tfd7tc-gpggmmc5d0og3a0/original/image.jpg"
                            className={cx('logo-page')}
                        />
                    </div>
                    <Menu>
                        <MenuItem title={t('about')} to={config.routes.view} icon={<FontAwesomeIcon icon={faDashboard} />} />
                        <MenuItem title={t('baoCao')} to={config.routes.baocao} icon={<FontAwesomeIcon icon={faFileExcel} />} />
                        <MenuItem
                            title={t('dsKH')}
                            to={config.routes.home}
                            icon={<FontAwesomeIcon icon={faUser} />}
                        />
                        <MenuItem title={t('caiDat')} to={config.routes.caidat} icon={<FontAwesomeIcon icon={faSlideshare} />} />
                    </Menu>
                </aside>

        </>
    );
}

export default Nav;
