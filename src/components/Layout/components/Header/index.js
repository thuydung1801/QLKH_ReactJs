// import config from '~/config';
import classNames from 'classnames/bind';
import styles from './Header.module.scss';

const cx = classNames.bind(styles);

function Header() {
    return (
        <header className={cx('wrapper')}>
            <div className={cx('inner')}>
                <h2 className={cx('name-page')}>DgDg</h2>
                <div className={cx('combo-user')}>
                    <img
                        src="https://cpad.ask.fm/450/774/576/-29996968-1tfd7tc-gpggmmc5d0og3a0/original/image.jpg"
                        className={cx('avatar')}
                        alt="Nong Thi Thuy Dung"
                    />
                    <div className={cx('header-right')}>Nông Thị Thùy Dung</div>
                </div>
            </div>
        </header>
    );
}

export default Header;
