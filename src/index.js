import React from 'react';
import ReactDOM from 'react-dom/client';
// DarkTheme
import {ThemeProvider} from 'theme-ui'
import theme from '~/pages/CaiDat/theme';

import { useTranslation } from 'react-i18next'
import axios from 'axios';

import App from '~/App';
import GlobalStyles from './components/GlobalStyles';
import reportWebVitals from './reportWebVitals';

import { Suspense } from 'react';
import '../src/pages/CaiDat/i18n.js'

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
    <React.StrictMode>
        {/*<Suspense fallback="...loading">*/}
        <ThemeProvider  theme={theme}>
            {/*<Suspense fallback="...loading">*/}
                <GlobalStyles>
                    <App />
                </GlobalStyles>
            {/*</Suspense>*/}

        </ThemeProvider>
        {/*</Suspense>*/}

    </React.StrictMode>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
