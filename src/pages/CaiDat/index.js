import { useEffect, useState } from 'react';
import React from 'react';
import {useTranslation} from 'react-i18next';

/** @jsxImportSource theme-ui*/
import './i18n'


import {useColorMode} from 'theme-ui';

//
const languages = [
    { value: '', text: "Options" },
    { value: 'en', text: "English" },
    { value: 'vi', text: "Vietnamese" },

    { value: 'bn', text: "Bengali" },
    { value: 'hy', text: "Armenian" }
]
function CaiDat() {
    //DarkTheme
    const [colorMode, setColorMode] = useColorMode();
    //
    // // Language

    const { t } = useTranslation();
    const [lang, setLang] = useState('');
    //
    const handleChange = e => {
        setLang(e.target.value);
        let loc = "http://172.16.10.97:3000/";
        window.location.replace(loc + "?lng=" + e.target.value);
    }

    return (
        <>
            <button
                onClick={() => setColorMode(colorMode === 'light' ? 'dark' : 'light')}>{t('toggle')} {colorMode === 'light' ? '🌙' : '☀'}</button>

            <label>{t('choose')}</label>
            <select value={lang} onChange={handleChange}>
                {languages.map(item => {
                    return (<option key={item.value}
                                    value={item.value}>{item.text}</option>);
                })}
            </select>


        </>
    )
}
export default CaiDat