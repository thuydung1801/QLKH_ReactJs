const theme = {
    initialColorModeName:'light',
    colors: {
        text: "black",
        inverseText :'white',
        background: 'white',
        primary: '#01408e',
        modes: {
            dark:{
                text: "#1e1d1d",
                inverseText :'black',
                background: '#5b5a5a',
                primary: '#4b4c4d',

                // text: "#4b4949",
                // inverseText :'black',
                // background: '#707273',
                // primary: '#77797a',

            }
        }
    }
}
export default theme