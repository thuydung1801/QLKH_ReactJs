/* eslint-disable no-undef */
// import config from '~/config';
import classNames from 'classnames/bind';
import styles from './Home.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleLeft, faAngleRight, faAnglesLeft, faAnglesRight, faDownload } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import react, { useEffect, useState } from 'react';

import Moment from 'moment';
import fileDownload from 'js-file-download';

import { CSVLink, CSVDownload } from 'react-csv';
import { useTranslation } from 'react-i18next';


const cx = classNames.bind(styles);

function Home() {
    const [data, setData] = useState([]);
    const [numberData, setNumberData] = useState([]);

    const [page, setPage] = useState(1);

    const {t} = useTranslation();
    useEffect(() => {
        getUsers(page);

        // split data =>  n page, lay page thu i
        // const PAGE_SIZE = 8
        // let tmp =[]
        // for(let i=PAGE_SIZE*(page-1);i<PAGE_SIZE*(page);i++){
        //     tmp.push(data[i])
        // }
        // setViewData(tmp)

    }, [page]);

    useEffect(()=>{
        getAll();
    },[''])
    const getUsers = async (page) => {
        const response = await axios.get(`http://localhost:4000/customer?page=${page}`);
        if (response.status === 200) {
            setData(response.data);
        }
    };

    const getAll = async () => {
        const response = await axios.get('http://localhost:4000/customer');
        if (response.status === 200) {
            setNumberData(response.data);
        }
        console.log(numberData)
    };

    const nextPage = () => {
        if (data.length < 16) return;
        setPage(prev => prev + 1);
    };
    const previewPage = () => {
        if (page == 1) return;

        setPage(prev => prev - 1);
    };


    //--------DownLoad

    const headers = [
    {label: 'Ma', key: 'code'},
    {label: 'Ten', key: 'name'},
    {label: 'Ngay sinh', key: 'dateOfBirth'},
    {label: 'mobile', key: 'mobile'},
    {label: 'Address', key: 'address'},
    {label: 'Employee', key: 'Employee.name'},
    {label: 'Category', key: 'Category.name'},

    ];


    const csvLink = {
        filename:"file.csv",
        headers:headers,
        data: data
        }

    //------------

return (
        <>
            {/* <h2>Home page</h2>
            <h4>Chưa có nội dung</h4> */}
            {/* CONTENT TITLE */}
            <div className={cx('content-tittle')}>
                <div className={cx('tittle-left')}>
                    <div className={cx('m-tittle-l-text')}>{t('dsKH')}</div>
                </div>
                <div className={cx('tittle-right')}>
                    <div className='row'>

                        <CSVLink {...csvLink}>
                            <>
                                <button className='m-btn-text' >
                                    <div>{<FontAwesomeIcon icon={faDownload} />} </div>
                                    <div className='btn-download'>{t('taiXuong')}</div>
                                </button>
                            </>
                            {/*export*/}
                        </CSVLink>


                    </div>
                </div>
            </div>


            {/*CONTENT TABLE bảng dữ liệu */}
            <div className={cx('content-table')}>
                <div className={cx('row')}>
                    <div className={cx('grid')}>
                        <table className={cx('table')} border='1'>
                            <thead>
                            <tr>
                                <th className={cx('text-align-left')}>{t('maKH')}</th>
                                <th className={cx('text-align-left')}>{t('tenKH')}</th>
                                <th className={cx('text-align-center')}>{t('ngaySinh')}</th>
                                <th className={cx('text-align-center')}>{t('dienThoai')}</th>
                                <th className={cx('text-align-left')}>{t('diaChi')}</th>
                                <th className={cx('text-align-left')}>{t('nVien')}</th>
                                <th className={cx('text-align-left')}>{t('loaiKH')}</th>
                            </tr>
                            </thead>
                            <tbody>
                            {data && data.map((item, index) => {
                                return (
                                    <tr key={index}>
                                        <td className={cx('text-align-left')}>{item.code}</td>
                                        <td className={cx('text-align-left')}>{item.name}</td>
                                        <td className={cx('text-align-center')}>{Moment(item.dateOfBirth).format('DD-MM-YYYY')}</td>
                                        <td className={cx('text-align-center')}>{item.mobile}</td>
                                        <td className={cx('text-align-left')}>{item.address}</td>
                                        <td className={cx('text-align-left')}>{item.Employee.name}</td>
                                        <td className={cx('text-align-left')}>{item.Category.name}</td>
                                    </tr>

                                );
                            })}


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {/* Phân trang */}

            <div className={cx('m-table-paging')}>
                <div className={cx('m-paging-left')}>{t('hienThi')} {(page*16 - 15)}-{(page*16) > numberData.length ? numberData.length : (page*16)}/{numberData.length} {t('KH')}</div>

                <div className={cx('m-paging-center')}>

                    <div className={cx('m-paging-prev')} onClick={previewPage}>
                        {<FontAwesomeIcon icon={faAngleLeft} />}
                    </div>

                    {page > 1 && <div className={cx('m-paging-number')} onClick={previewPage}>
                        {page - 1}
                    </div>
                    }

                    <div className={cx('m-paging-number')}>
                        {page}

                    </div>
                    {data.length >=16 && <div className={cx('m-paging-number')} onClick={nextPage}>
                        {page + 1}
                    </div>}


                    <div onClick={nextPage} className={cx('m-paging-next')}>
                        {<FontAwesomeIcon icon={faAngleRight} />}
                    </div>

                </div>
            </div>


        </>
    );
}

export default Home;
