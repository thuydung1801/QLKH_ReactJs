import {  useEffect } from 'react';
import {useState} from "react"
import classNames from 'classnames/bind';
import BarChart from '~/pages/View/BarChart';

import axios from 'axios';
import { arrays2csv } from 'react-csv/lib/core';
import LineChart from '~/pages/View/LineChart';
import PieChart from '~/pages/View/PieChart';
import { useTranslation } from 'react-i18next';

import styles from './View.module.scss'

const cx = classNames.bind(styles);
function View(){
    const {t}=useTranslation();
    //--get All data
    const [data, setData] = useState([]);
    //--get Employee
    const [dataEpl, setDataEpl] = useState([]);
    const epl=dataEpl.map((dt) =>dt.name)
    console.log(epl)

    //--get data in Chart
    const [userData, setUserData] =useState({
        labels :epl,
        datasets :[{
            label : "Khách hàng",
            data : []
        }]

    });
    useEffect(()=>{
        getAll();
    },[])

    useEffect(()=>{
        getEpl();
    },[])
    const getEpl = async ()=>{
        const response = await axios.get('http://localhost:4000/category')
        if (response.status === 200) {
            setDataEpl(response.data);
        }
    }


    const getAll = async () => {
        const response = await axios.get('http://localhost:4000/customer');
        if (response.status === 200) {
            setData(response.data);
        }

    };
    console.log(data)
    useEffect(()=>{
        let nvDung=0, nvKien =0, nvLinh=0
        let VIP=0, Thuong=0, TN=0, Moi=0
        const getCountEpl = ()=>{

            // for ( let i =0;i<data.length;i++){
            //     if(data[i].Employee.name === "Nông Thị Thùy Dung") nvDung++;
            //     else if(data[i].Employee.name === "Nguyễn Trần Kiên") nvKien++;
            //     else if(data[i].Employee.name === "Hoàng Thị Mỹ Linh") nvLinh++;
            // }
            for ( let i =0;i<data.length;i++){
                if(data[i].Category.name === "Khách hàng VIP") VIP++;
                else if(data[i].Category.name === "Khách hàng thường") Thuong++;
                else if(data[i].Category.name === "Khách hàng tiềm năng") TN++;
                else if(data[i].Category.name === "Khách hàng mới") Moi++;
            }

        }

        getCountEpl()
        // let dataEpl = [nvDung,nvKien,nvLinh]
        let dataEpl = [VIP, Thuong,TN,Moi]

        // console.log(dataEpl)
        setUserData({
            labels :epl,
            datasets :[{
                label : "Employee",
                data : dataEpl,
                backgroundColor: [
                    "rgba(75,192,192,1)" , "#50AF95","#f3ba2f","#e86e6e",
                ],
                borderColor : "black",
                borderWidth : 2


            }]

        })

    },[data])
    console.log("userdata",userData)


    return(
        <>

            {/*<div className={cx('view')}>*/}
                <div style={{padding: 50}}>{t('slKhachHang')}</div>

                <div style={{float:'left'}}>
                    <div style={{width: 500}} >

                        <BarChart chartData={userData}/>
                    </div>
                    <div style={{width: 500}} >
                        <LineChart chartData={userData}/>
                    </div>
                </div>

                <div style={{float:'right'}} className={cx('pie')} style={{width:300}}>
                    <PieChart  chartData={userData}/>
                </div>
            {/*</div>*/}

        </>
    )
}
export default View