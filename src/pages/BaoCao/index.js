import { useTranslation } from 'react-i18next';

function BaoCao() {
    const {t} = useTranslation();
    return (
        <>
            <h2>{t('trangBC')}</h2>
            <h4>{t('ndBaoCao')}</h4>
        </>
    );
}

export default BaoCao;
